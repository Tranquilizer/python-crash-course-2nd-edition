#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest

from city_functions import format_location

class LocationTestCase(unittest.TestCase):
    def test_city_country(self):
        self.assertEqual(format_location('city', 'country'),
                         'City, Country')
        
    def test_city_country_population(self):
        self.assertEqual(format_location('city', 'country', 1337),
                         'City, Country - population 1337')
        
if __name__ == '__main__':
    unittest.main()