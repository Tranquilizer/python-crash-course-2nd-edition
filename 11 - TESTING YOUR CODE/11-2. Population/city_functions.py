#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def format_location(city, country, population=''):
    if population:
        return f'{city.title()}, ' \
            f'{country.title()} - population {population}'
    else:
        return f'{city.title()}, {country.title()}'