#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest

from employee import Employee

class EmployeeTestCase(unittest.TestCase):
    def setUp(self):
        first_name = 'John'
        last_name = 'Doe'
        annual_salary = 2500
        
        self.employee = Employee(first_name, last_name, annual_salary)
        self.salary_raise = 2500
    
    def test_give_default_raise(self):
        self.employee.give_raise()
        
        self.assertEqual(self.employee.annual_salary, 7500)
    
    def test_give_custom_raise(self):
        self.employee.give_raise(self.salary_raise)
        
        self.assertEqual(self.employee.annual_salary, 5000)

if __name__ == '__main__':
    unittest.main()