#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def format_location(city, country):
    return f'{city.title()}, {country.title()}'