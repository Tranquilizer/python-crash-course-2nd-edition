#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest

from city_functions import format_location

class LocationTestCase(unittest.TestCase):
    def test_city_country(self):
        self.assertEqual(format_location('city', 'country'),
                         'City, Country')

if __name__ == '__main__':
    unittest.main()