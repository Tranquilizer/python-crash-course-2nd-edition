#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cars
from cars import make_car
from cars import make_car as mc
import cars as c
from cars import *

car = cars.make_car('subaru', 'outback', color='blue', tow_package=True)
print(car)
car = make_car('subaru', 'outback', color='blue', tow_package=True)
print(car)
car = mc('subaru', 'outback', color='blue', tow_package=True)
print(car)
car = c.make_car('subaru', 'outback', color='blue', tow_package=True)
print(car)