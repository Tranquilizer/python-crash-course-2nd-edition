#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def make_shirt(size='l', text='I love Python'):
    print(size.title() + '-sized shirt with "' + text +
          '" printed on it')
    
make_shirt('l')
make_shirt('m')
make_shirt(text='I\'m with stupid')