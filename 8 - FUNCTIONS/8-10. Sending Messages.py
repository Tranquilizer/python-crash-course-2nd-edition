#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def show_messages(messages):
    for message in messages:
        print(message)

def send_messages(messages, sent_messages):
    while messages:
        message = messages.pop(0)
        print(message)
        sent_messages.append(message)

messages = ['First message', 'Second message', 'Third message']
sent_messages = []

send_messages(messages, sent_messages)
print(messages)
print(sent_messages)