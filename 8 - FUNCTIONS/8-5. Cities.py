#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def describe_city(name, country='Russia'):
    print(name.title() + ' is in ' + country.title())
    
describe_city('Moscow')
describe_city('Reykjavik', 'Iceland')
describe_city('Saint Petersburg')