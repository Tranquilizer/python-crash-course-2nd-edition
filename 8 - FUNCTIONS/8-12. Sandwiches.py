#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def print_sandwich_items(*items):
    print('Summary of the sandwich that’s being ordered:')
    for item in items:
        print(f'\t- {item}')

print_sandwich_items('bolts')
print_sandwich_items('pineapples', 2, 'spoons')
print_sandwich_items()