#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def make_album(artist, album, number_of_songs=None):
    result_album = {}
    
    result_album['artist name'] = artist.title()
    result_album['album title'] = album.title()
    if number_of_songs:
        result_album['number of songs'] = number_of_songs
    
    return result_album

while True:
    print('To quit write \'quit\', duh')
    artist = input('Artist name: ')
    if artist == 'quit':
        break
    album = input('Album title: ')
    if album == 'quit':
        break
    print(make_album(artist, album))