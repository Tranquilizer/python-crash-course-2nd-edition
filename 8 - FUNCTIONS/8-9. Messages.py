#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def show_messages(messages):
    for message in messages:
        print(message)

messages = ['First message', 'Second message', 'Third message']

show_messages(messages)