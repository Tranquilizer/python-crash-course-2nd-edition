#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def city_country(name, country):
    return f'{name}, {country}'.title()

print(city_country('Moscow', 'Russia'))
print(city_country('Oslo', 'Norway'))
print(city_country('Madrid', 'Spain'))