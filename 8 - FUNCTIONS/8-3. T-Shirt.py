#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def make_shirt(size, text):
    print(size.title() + '-sized shirt with "' + text +
          '" printed on it')
    
make_shirt('m', 'I\'m with stupid')
make_shirt(text='I\'m with stupid', size='m')