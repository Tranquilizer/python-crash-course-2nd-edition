#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def make_car(manufacturer, model, **other_info):
    other_info['manufacturer'] = manufacturer
    other_info['model'] = model
    
    return other_info

car = make_car('subaru', 'outback', color='blue', tow_package=True)
print(car)