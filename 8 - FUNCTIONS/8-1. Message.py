#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def display_message():
    print('In this chapter I’ll learn to write functions, which ' \
          'are named blocks of code that are designed to do one ' \
          'specific job.')
    
display_message()