#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def make_album(artist, album, number_of_songs=None):
    result_album = {}
    
    result_album['artist name'] = artist.title()
    result_album['album title'] = album.title()
    if number_of_songs:
        result_album['number of songs'] = number_of_songs
    
    return result_album

print(make_album('megadeth', 'rust in peace'))
print(make_album('gorillaz', 'demon days'))
print(make_album('tool', 'lateralus'))
print(make_album('within temptation', 'hydra', 2014))