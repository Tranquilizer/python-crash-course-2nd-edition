#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def favorite_book(title):
    print(f' One of my favorite books is {title.title()}.')
    
favorite_book('The Count of Monte Cristo')