#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def build_profile(first, last, **user_info):
    """Build a dictionary containing everything we know about a user."""
    user_info['first_name'] = first
    user_info['last_name'] = last
    return user_info

user_profile = build_profile(
        'alexander', 'kuzmin', sex='male',
        height=177, weight=73)
print(user_profile)