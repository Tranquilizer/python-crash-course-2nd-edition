#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

import pygame
from pygame.sprite import Sprite

class Raindrop(Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load('raindrop.bmp')
        self.rect = self.image.get_rect()
    
    def update(self):
        self.rect.y += 1
        if self.rect.y > 800:
            self.rect.y = -50

def update():
    raindrops.update()
    screen.fill((0, 0, 0))
    raindrops.draw(screen)
    pygame.display.flip()

screen = pygame.display.set_mode((1200, 800))
raindrops = pygame.sprite.Group()
for i in range(0, 29):
    raindrop = Raindrop()
    raindrop.rect.x = 40 * i
    raindrop.rect.y = -50
    raindrops.add(raindrop)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
    update()