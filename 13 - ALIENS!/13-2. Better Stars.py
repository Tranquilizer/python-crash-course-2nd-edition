#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from random import randint

import pygame
from pygame.sprite import Sprite

def _create_star(screen, stars, star_number, row_number):
    star = Star(screen)
    star_width, star_height = star.rect.size
    star.x = randint(0, 1150)
    star.rect.x = star.x
    star.rect.y = randint(0, 750)
    stars.add(star)

class Star(Sprite):
    def __init__(self, ai_game):
        super().__init__()
        self.screen = ai_game
        
        self.image = pygame.image.load('star.bmp')
        self.rect = self.image.get_rect()
        
        self.rect.x = self.rect.width
        self.rect.y = self.rect.height
        
        self.x = float(self.rect.x)

screen = pygame.display.set_mode((1200, 800))
pygame.display.set_caption("Stars")
stars = pygame.sprite.Group()

star = Star(screen)
star_width, star_height = star.rect.size

available_space_x = 1200 
number_stars_x = available_space_x // star_width

available_space_y = 800
number_rows = available_space_y // star_height

for row_number in range(number_rows):
    for star_number in range(number_stars_x):
        _create_star(screen, stars, star_number, row_number)
        
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
    stars.draw(screen)
    pygame.display.flip()