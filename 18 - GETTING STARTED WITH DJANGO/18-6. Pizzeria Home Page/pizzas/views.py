from django.shortcuts import render

def  index(request):
    """The home page of Pizzas"""
    return render(request, 'pizzas/index.html')