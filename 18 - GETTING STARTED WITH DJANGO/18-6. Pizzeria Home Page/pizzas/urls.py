#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Defines URL patterns for learning_logs."""

from django.urls import path

from . import views

app_name = 'pizzas'
urlpatterns = [
    # Home page
    path('', views.index, name='index'),
]