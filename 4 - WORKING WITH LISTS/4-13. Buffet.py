#!/usr/bin/env python3
# -*- coding: utf-8 -*-

foods = ('pizza', 'falafel', 'carrot cake', 'cannoli', 'ice cream')

for food in foods:
    print(food)

foods = ('pizza', 'falafel 2', 'carrot cake', 'cannoli 2', 'ice cream')

for food in foods:
    print(food)
    
foods[0] = 'pizza 2'