#!/usr/bin/env python3
# -*- coding: utf-8 -*-

animals = ["orca", "bear", "crocodile"]

for animal in animals:
    print(f"{animal.title()} is pretty cool.")

print("Any of these animals would NOT make a great pet!")