#!/usr/bin/env python3
# -*- coding: utf-8 -*-

pizzas = ["pepperoni", "hawaiian", "marinara"]

for pizza in pizzas:
    print(f"{pizza.title()} pizza is nice.")

print("Yeah, pizza is great.")