#!/usr/bin/env python3
# -*- coding: utf-8 -*-

cubes = [i ** 3 for i in range(1, 11)]

for cube in cubes:
    print(cube)