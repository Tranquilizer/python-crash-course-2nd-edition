#!/usr/bin/env python3
# -*- coding: utf-8 -*-

cubes = [i ** 3 for i in range(1, 11)]

for cube in cubes:
    print(cube)
    
print("The first three items in the list are:")
for cube in cubes[:3]:
    print(cube)
print("Three items from the middle of the list are:")
for cube in cubes[3:6]:
    print(cube)
print("The last three items in the list are:")
for cube in cubes[-3:]:
    print(cube)