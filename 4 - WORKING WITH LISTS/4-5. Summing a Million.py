#!/usr/bin/env python3
# -*- coding: utf-8 -*-

numbers = []

for i in range(1, 1_000_001):
    numbers.append(i)
    
print(min(numbers))
print(max(numbers))
print(sum(numbers))