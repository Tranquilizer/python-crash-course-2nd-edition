#!/usr/bin/env python3
# -*- coding: utf-8 -*-

odd_numbers = []

for i in range (1, 21, 2):
    odd_numbers.append(i)
    
for number in odd_numbers:
    print(number)