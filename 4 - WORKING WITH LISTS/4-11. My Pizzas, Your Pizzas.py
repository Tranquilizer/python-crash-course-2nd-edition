#!/usr/bin/env python3
# -*- coding: utf-8 -*-

pizzas = ["pepperoni", "hawaiian", "marinara"]
friend_pizzas = pizzas[:]

pizzas.append("neapolitan")
friend_pizzas.append("seafood")

print("My favorite pizzas are:")
for pizza in pizzas:
    print(pizza)
print("My friend’s favorite pizzas are:")
for pizza in friend_pizzas:
    print(pizza)