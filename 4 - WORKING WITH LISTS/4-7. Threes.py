#!/usr/bin/env python3
# -*- coding: utf-8 -*-

multiples_of_three = [i for i in range(3, 31, 3)]

for multiple in multiples_of_three:
    print(multiple)