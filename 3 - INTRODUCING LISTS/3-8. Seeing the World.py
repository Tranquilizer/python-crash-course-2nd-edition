#!/usr/bin/env python3
# -*- coding: utf-8 -*-

locations = ["Norway", "China", "New Zealand", "Mexico"]

print(locations)
print(sorted(locations))
print(locations)
print(sorted(locations, reverse=True))
print(locations)
locations.reverse()
print(locations)
locations.reverse()
print(locations)
locations.sort()
print(locations)
locations.sort(reverse=True)
print(locations)