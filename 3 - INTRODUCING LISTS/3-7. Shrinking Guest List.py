#!/usr/bin/env python3
# -*- coding: utf-8 -*-

guests = ["No", "Bo", "Dy"]

guests.insert(0, "More")
guests.insert(2, "Nob")
guests.append("Ody")

for guest in guests:
    print(f"Hey {guest}. Come over, we'll have a dinner.")
    
print("Sorry, we can actually invite only two people, lol.")

for i in range(len(guests) - 2):
    guests.pop()

for guest in guests:
    print(f"{guest}, you can still come.")