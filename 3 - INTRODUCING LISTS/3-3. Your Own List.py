#!/usr/bin/env python3
# -*- coding: utf-8 -*-

transportation_types = ["subway", "bicycle", "bus"]

for transport in transportation_types:
    print(f"{transport.title()} is a great way to move around.")