#!/usr/bin/env python3
# -*- coding: utf-8 -*-

guests = ["No", "Bo", "Dy"]

for guest in guests:
    print(f"Hey {guest}. Come over, we'll have a dinner.")