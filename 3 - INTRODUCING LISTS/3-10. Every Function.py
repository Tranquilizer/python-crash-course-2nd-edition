#!/usr/bin/env python3
# -*- coding: utf-8 -*-

words = ["this", "is", "a", "\"great\"", "exercise"]

print(words[0])
print(words[-1])
print(words)
words[3] = "meh"
print(words)
words.append("obviously")
print(words)
words.insert(4, "little")
print(words)
del words[4]
print(words)
words.pop()
print(words)
words.pop(4)
print(words)
words.remove("a")
print(words)
print(sorted(words))
print(sorted(words, reverse=True))
print(words)
words.sort()
print(words)
words.sort(reverse=True)
print(words)
words.reverse()
print(words)
print(len(words))