#!/usr/bin/env python3
# -*- coding: utf-8 -*-

names = ["Nastya", "Zhenek", "Serega"]

for name in names:
    print(f"Hey {name}.\nWhat's going on?")