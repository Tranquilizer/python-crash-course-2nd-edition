#!/usr/bin/env python3
# -*- coding: utf-8 -*-

guests = ["No", "Bo", "Dy"]

missing_guest = guests.pop(1)
guests.insert(1, "Another Bo")

for guest in guests:
    print(f"Hey {guest}. Come over, we'll have a dinner.")

print(f"... And {missing_guest} couldn't make it.")