#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
from datetime import datetime
import matplotlib.pyplot as plt

with open("sitka_weather_2018_simple.csv") as file:
    reader = csv.reader(file)
    header = next(reader)
    prcps, dates = [], []
    for row in reader:
        dates.append(datetime.strptime(row[2], '%Y-%m-%d'))
        prcps.append(float(row[3]))

plt.style.use('seaborn')
fig, ax = plt.subplots()
ax.plot(dates, prcps, c='red')
plt.title("Daily rainfall amounts, 2018", fontsize=24)
plt.xlabel('', fontsize=16)
fig.autofmt_xdate()
plt.ylabel("PRCP", fontsize=16)
plt.tick_params(axis='both', which='major', labelsize=16)
plt.show()