#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
from datetime import datetime
import matplotlib.pyplot as plt

def get_data(filename):
    with open(filename) as file:
        reader = csv.reader(file)
        next(reader)
        dates, highs, lows = [], [], []
        for row in reader:
            try:
                date = datetime.strptime(row[2], '%Y-%m-%d')
                high = float(row[5])
                low = float(row[6])
            except ValueError:
                pass
            else:
                dates.append(date)
                highs.append(high)
                lows.append(low)
    
    return [dates, highs, lows]

plt.style.use('seaborn')
fig, ax = plt.subplots()
plt.title("Daily high and low temperatures", fontsize=24)
plt.xlabel('', fontsize=16)
fig.autofmt_xdate()
plt.ylabel("Temperature (F)", fontsize=16)
plt.tick_params(axis='both', which='major', labelsize=16)
for name in ("sitka_weather_2018_simple.csv", \
             "death_valley_2018_simple.csv"):
    data = get_data(name)
    
    ax.plot(data[0], data[1], c='red', alpha=0.5)
    ax.plot(data[0], data[2], c='blue', alpha=0.5)
    plt.fill_between(data[0], data[1], data[2], facecolor='blue', alpha=0.1)
plt.show()