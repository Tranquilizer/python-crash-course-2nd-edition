#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv

from plotly.graph_objs import Scattergeo, Layout
from plotly import offline

# Explore the structure of the data.
with open('world_fires_1_day.csv') as file:
    reader = csv.reader(file)
    header = next(reader)
    lat_index = header.index('latitude')
    long_index = header.index('longitude')
    brightness_index = header.index('brightness')
    lat, long, bright = [], [], []
    for row in reader:
        lat.append(float(row[lat_index]))
        long.append(float(row[long_index]))
        bright.append(float(row[brightness_index]))

# Map the fires.
data = [{
    'type': 'scattergeo',
    'lon': long,
    'lat': lat,
    'marker': {
        'size': [abs(b/100) for b in bright],
        'color': bright,
        'colorscale': 'Viridis',
        'reversescale': True,
        'colorbar': {'title': 'Brightness'},
    },
}]
my_layout = Layout(title='Fires')

fig = {'data': data, 'layout': my_layout}
offline.plot(fig, filename='fires.html')