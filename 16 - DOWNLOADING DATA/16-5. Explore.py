#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
from datetime import datetime

import matplotlib.pyplot as plt

prcp_label = "PRCP"
date_label = "DATE"

with open("death_valley_2018_simple.csv") as file:
    reader = csv.reader(file)
    header = next(reader)
    prcp_index = header.index(prcp_label)
    date_index = header.index(date_label)
    prcps, dates = [], []
    for row in reader:
        dates.append(datetime.strptime(row[date_index], '%Y-%m-%d'))
        prcps.append(float(row[prcp_index]))

plt.style.use('seaborn')
fig, ax = plt.subplots()
ax.plot(dates, prcps, c='red')
plt.title("Daily rainfall amounts, 2018", fontsize=24)
plt.xlabel('', fontsize=16)
fig.autofmt_xdate()
plt.ylabel(prcp_label, fontsize=16)
plt.tick_params(axis='both', which='major', labelsize=16)
plt.show()