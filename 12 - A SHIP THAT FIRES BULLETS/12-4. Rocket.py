#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

import pygame

class Rocket:
    def __init__(self, ai_game):
        self.screen = ai_game.screen
        self.screen_rect = ai_game.screen.get_rect()
        
        self.original_image = pygame.image.load('rocket.bmp')
        self.image = self.original_image
        self.rect = self.image.get_rect()
        self.rect.center = self.screen_rect.center
        
        self.angle = 0

        self.moving_up = False
        self.moving_down = False
        self.moving_left = False
        self.moving_right = False
        
    def update(self):
        if self.moving_up and self.rect.top > 0:
            self.rect.y -= 1
        if (self.moving_down and
            self.rect.bottom < self.screen_rect.bottom):
            self.rect.y += 1
        if self.moving_left and self.rect.left > 0:
            self.rect.x -= 1
        if (self.moving_right and
            self.rect.right < self.screen_rect.right):
            self.rect.x += 1

    def blitme(self):
        self.screen.blit(self.image, self.rect)


class Game:
    def __init__(self):
        pygame.init()
        self.screen = pygame.display.set_mode((1200, 800))
        pygame.display.set_caption("Rocket")
        self.rocket = Rocket(self)
        self.bg_color = (230, 230, 230)
    
    def run_game(self):
        while True:
            self._check_events()
            self.rocket.update()
            self._update_screen()

    def _check_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                self._check_keydown_events(event)
            elif event.type == pygame.KEYUP:
                self._check_keyup_events(event)

    def _check_keydown_events(self, event):
        if event.key == pygame.K_RIGHT:
            self.rocket.moving_right = True
        elif event.key == pygame.K_LEFT:
            self.rocket.moving_left = True
        elif event.key == pygame.K_UP:
            self.rocket.moving_up = True
        elif event.key == pygame.K_DOWN:
            self.rocket.moving_down = True
        elif event.key == pygame.K_q:
            sys.exit()

    def _check_keyup_events(self, event):
        if event.key == pygame.K_RIGHT:
            self.rocket.moving_right = False
        elif event.key == pygame.K_LEFT:
            self.rocket.moving_left = False
        elif event.key == pygame.K_UP:
            self.rocket.moving_up = False
        elif event.key == pygame.K_DOWN:
            self.rocket.moving_down = False

    def _update_screen(self):
        self.screen.fill(self.bg_color)
        self.rocket.blitme()
        pygame.display.flip()

if __name__ == '__main__':
    game = Game()
    game.run_game()