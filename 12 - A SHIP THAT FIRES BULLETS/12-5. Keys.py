#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

import pygame

pygame.init()
pygame.display.set_mode((1200, 800))
pygame.display.set_caption("Keys")

while True:
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            print(event.key)
        if event.type == pygame.QUIT:
            sys.exit()