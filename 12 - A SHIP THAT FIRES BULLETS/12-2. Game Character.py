#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

import pygame

class Character:
    def __init__(self, ai_game):
        self.screen = ai_game.screen
        self.screen_rect = ai_game.screen.get_rect()
        
        self.image = pygame.image.load('star.bmp')
        self.rect = self.image.get_rect()
        self.rect.center = self.screen_rect.center
    
    def blitme(self):
        """Draw the ship at its current location."""
        self.screen.blit(self.image, self.rect)

class Game:
    def __init__(self):
        pygame.init()
        
        self.screen = pygame.display.set_mode((400, 300))
        pygame.display.set_caption("Game Character")
        self.bg_color = (64, 128, 192)
        self.character = Character(self)
    
    def run(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
            self.screen.fill(self.bg_color)
            self.character.blitme()
            pygame.display.flip()

if __name__ == '__main__':
    game = Game()
    game.run()