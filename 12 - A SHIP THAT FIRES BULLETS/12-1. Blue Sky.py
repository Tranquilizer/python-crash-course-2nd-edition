#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

import pygame

class Game:
    def __init__(self):
        pygame.init()
        
        self.screen = pygame.display.set_mode((400, 300))
        pygame.display.set_caption("Blue Sky")
        self.bg_color = (64, 128, 192)
    
    def run(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
            self.screen.fill(self.bg_color)
            pygame.display.flip()

if __name__ == '__main__':
    game = Game()
    game.run()