#!/usr/bin/env python3
# -*- coding: utf-8 -*-

while True:
    try:
        first_value = int(input('Enter first value: '))
        second_value = int(input('Enter second value: '))
    except ValueError:
        print('Value you entered is not a number')
        break
    else:
        print(f'Sum is: {first_value + second_value}')