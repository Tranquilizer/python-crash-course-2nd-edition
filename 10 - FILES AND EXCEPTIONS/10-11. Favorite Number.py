#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

number = int(input('Hi. What\'s your favorite number?\n'))

with open('favorite_number.json', 'w') as file:
    json.dump(number, file)
    
with open('favorite_number.json', 'r') as file:
    loaded = json.load(file)
    
print(f'I know your favorite number! It\'s {loaded}')