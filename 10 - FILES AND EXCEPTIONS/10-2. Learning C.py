#!/usr/bin/env python3
# -*- coding: utf-8 -*-

file = 'learning_python.txt'

with open(file) as file_object:
    for line in file_object:
        print(line.replace('Python', 'Java').rstrip())