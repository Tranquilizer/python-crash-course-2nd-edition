#!/usr/bin/env python3
# -*- coding: utf-8 -*-

with open('responses.txt', 'a') as file:
    while True:
        response = input('Why do you like programming? ')
        file.write(f'{response}\n')