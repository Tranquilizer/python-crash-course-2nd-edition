#!/usr/bin/env python3
# -*- coding: utf-8 -*-

with open('guest_book.txt', 'a') as file:
    while True:
        name = input('What\'s your name? ')
        print(f'Greetings {name}')
        file.write(f'{name}\n')