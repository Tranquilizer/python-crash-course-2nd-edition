#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

try:
    with open('favorite_number2.json', 'r') as file:
        number = json.load(file)
except FileNotFoundError:
    with open('favorite_number2.json', 'w') as file:
        json.dump(int(input('Hi. What\'s your favorite number? ')))
else:
    print(f'Your favorite number is {number}.')