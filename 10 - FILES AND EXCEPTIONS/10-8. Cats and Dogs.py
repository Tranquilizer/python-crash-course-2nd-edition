#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def read_file(name):
    try:
        with open(name, 'r') as file:
            print(file.read().rstrip())
    except FileNotFoundError:
        print(f'{name} not found')
    

name = 'cats.txt'
read_file(name)

name = 'dogs.txt'
read_file(name)