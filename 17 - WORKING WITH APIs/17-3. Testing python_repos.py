#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import unittest

class GitHubRepositories:
    def __init__(self, url, headers):
        self.url = url
        self.headers = headers
        
    def get_response(self):
        return requests.get(self.url, headers=self.headers)

class EmployeeTestCase(unittest.TestCase):
    def setUp(self):
        self.ghr = GitHubRepositories('https://api.github.com/search' \
                                      '/repositories?q=language:' \
                                      'python&sort=stars',
                                      {'Accept': 'application/' \
                                       'vnd.github.v3+json'})
        self.r = self.ghr.get_response()
    
    def test_give_default_raise(self):
        self.assertEqual(self.r.status_code, 200)
    
    def test_give_custom_raise(self):
        response_dict = self.r.json()
        
        self.assertEqual(len(response_dict['items']), 30)

if __name__ == '__main__':
    unittest.main()