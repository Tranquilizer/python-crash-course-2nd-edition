#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This program prints Linus Torvalds' quote

famous_person = "Linus Torvalds"
message = "I'm doing a (free) operating system (just a hobby, won't be big and professional like gnu) for 386(486) AT clones."

print(f"{famous_person.title()} once said, \"{message}\"")