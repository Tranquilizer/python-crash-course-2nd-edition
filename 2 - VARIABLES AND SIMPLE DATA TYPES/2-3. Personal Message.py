#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This program prints welcome message

name = "Sasha"

print(f"Hey {name}.\nWhat's going on?")