#!/usr/bin/env python3
# -*- coding: utf-8 -*-

name = "\n\t Sasha \t\n"

print(name)
print("---")
print(name.lstrip())
print("---")
print(name.rstrip())
print("---")
print(name.strip())