#!/usr/bin/env python3
# -*- coding: utf-8 -*-

name = "Sasha"

print(name.lower() + '\n' + name.upper() + '\n' + name.title())