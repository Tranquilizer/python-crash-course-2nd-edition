#!/usr/bin/env python3
# -*- coding: utf-8 -*-

author = "Linus Torvalds"
quote = "I'm doing a (free) operating system (just a hobby, won't be big and professional like gnu) for 386(486) AT clones."

print(author.title() + " once said, \"" + quote + "\"")