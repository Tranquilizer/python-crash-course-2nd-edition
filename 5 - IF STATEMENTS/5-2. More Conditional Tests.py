#!/usr/bin/env python3
# -*- coding: utf-8 -*-

first_string = "First string"
second_string = "Second string"
lower_first_string = "first string"
print(f"Is first string equals second: {first_string == second_string}")
print(f"Is it not: {first_string != second_string}")
print(f"Test of lower(): {first_string.lower() == lower_first_string}")

first_number = 5
second_number = 8
print(f"Is first number equals second: {first_number == second_number}")
print(f"Is it not: {first_number != second_number}")
print(f"Is it greater: {first_number > second_number}")
print(f"Is it greater or equals: {first_number >= second_number}")
print(f"Is it less: {first_number < second_number}")
print(f"Is it less or equals: {first_number <= second_number}")

third_number = 13
print(f"Test 'and': \
      {second_number > first_number and second_number > third_number}")
print(f"Test 'or': \
      {second_number > first_number or second_number > third_number}")

list = [1, 3, 5]
number = 3
if number in list:
    print(f"Test if number is in the list")
    number = 4
if number not in list:
    print(f"Test if it is not")