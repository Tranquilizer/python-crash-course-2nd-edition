#!/usr/bin/env python3
# -*- coding: utf-8 -*-

favorite_fruits = ['bananas', 'oranges', 'apples']

if 'bananas' in favorite_fruits:
    print('You really like bananas!')
if 'mangos' in favorite_fruits:
    print('You really like mangos!')
if 'oranges' in favorite_fruits:
    print('You really like oranges!')
if 'lemons' in favorite_fruits:
    print('You really like lemons!')
if 'apples' in favorite_fruits:
    print('You really like apples!')