#!/usr/bin/env python3
# -*- coding: utf-8 -*-

alien_color = "green"

if alien_color == "green":
    print("+5")
elif alien_color == "yellow":
    print("+10")
elif alien_color == "red":
    print("+15")

alien_color = "yellow"

if alien_color == "green":
    print("+5")
elif alien_color == "yellow":
    print("+10")
elif alien_color == "red":
    print("+15")

alien_color = "red"

if alien_color == "green":
    print("+5")
elif alien_color == "yellow":
    print("+10")
elif alien_color == "red":
    print("+15")