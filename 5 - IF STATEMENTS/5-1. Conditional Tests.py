#!/usr/bin/env python3
# -*- coding: utf-8 -*-

car = 'subaru'
print("Is car == 'subaru'? I predict True.")
print(car == 'subaru')
print("\nIs car == 'audi'? I predict False.")
print(car == 'audi')

print('---')

pizza = 'hawaiian'
print("Is pizza == 'hawaiian'? I predict True.")
print(pizza == 'hawaiian')
print("\nIs pizza == 'pepperoni'? I predict False.")
print(pizza == 'pepperoni')

print('---')

animal = 'cat'
print("Is animal == 'cat'? I predict True.")
print(animal == 'cat')
print("\nIs animal == 'dog'? I predict False.")
print(animal == 'dog')

print('---')

furniture = 'table'
print("Is furniture == 'table'? I predict True.")
print(furniture == 'table')
print("\nIs furniture == 'chair'? I predict False.")
print(furniture == 'chair')

print('---')

building = 'hospital'
print("Is building == 'hospital'? I predict True.")
print(building == 'hospital')
print("\nIs building == 'school'? I predict False.")
print(building == 'school')