#!/usr/bin/env python3
# -*- coding: utf-8 -*-

current_users = ['admin', 'sarcher', 'lkane', 'cfiggis', 'Ctunt']
new_users = ['cTunt', 'ppoovey', 'akrieger', 'rgillette', 'awoodhouse']

lower_current_users = []
for user in current_users:
    lower_current_users.append(user.lower())

for user in new_users:
    if user.lower() in lower_current_users:
        print('You need to enter new username')
    else:
        print('Username is available')