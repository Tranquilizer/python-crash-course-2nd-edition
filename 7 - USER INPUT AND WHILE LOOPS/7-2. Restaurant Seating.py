#!/usr/bin/env python3
# -*- coding: utf-8 -*-

number = input('How many people are in your dinner group? ')
number = int(number)

if number > 8:
    print('You\'ll have to wait for a table')
else:
    print('Your table is ready')