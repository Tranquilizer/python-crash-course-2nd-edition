#!/usr/bin/env python3
# -*- coding: utf-8 -*-

car = input('What rental car do you want? ')

print(f'Let me see if I can find you a {car.title()}.')