#!/usr/bin/env python3
# -*- coding: utf-8 -*-

prompt = '\nPlease enter pizza toppings:' \
'\n(Enter \'quit\' when you are finished.) '

while True:
    topping = input(prompt)
    if topping == 'quit':
        break
    else:
        print(f"I like {topping.lower()} on pizza!")