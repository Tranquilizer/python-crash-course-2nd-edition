#!/usr/bin/env python3
# -*- coding: utf-8 -*-

prompt = 'Hello. State your age (or \'quit\' to exit): '

while True:
    age = input(prompt)
    if age == 'quit':
        break
    age = int(age)
    if age < 3:
        print('Ticket is free')
    elif age < 13:
        print('Ticket costs 10$')
    else:
        print('Ticket costs 15$')