#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class User:
    def __init__(self, first_name, last_name, gender, birthday):
        self.first_name = first_name
        self.last_name = last_name
        self.gender = gender
        self.birthday = birthday
    
    def describe_user(self):
        print(f'First name: {self.first_name}')
        print(f'Last name: {self.last_name}')
        print(f'Gender: {self.gender}')
        print(f'Birthday: {self.birthday}')
    
    def greet_user(self):
        print(f'Hello {self.first_name.title()}. What\'s going on?')

class Privilege:
    def __init__(self, privileges):
        self.privileges = privileges
    
    def show_privileges(self):
        print('List of admin\'s privileges:')
        for privilege in self.privileges:
            print(f'\t- {privilege}')
            
class Admin(User):
    def __init__(self, first_name, last_name,
                 gender, birthday, privileges):
        super().__init__(first_name, last_name,gender, birthday)
        self.privileges = privileges