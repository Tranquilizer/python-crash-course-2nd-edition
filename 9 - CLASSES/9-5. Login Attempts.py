#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class User:
    def __init__(self, first_name, last_name, gender, birthday):
        self.first_name = first_name
        self.last_name = last_name
        self.gender = gender
        self.birthday = birthday
        self.login_attempts = 0
    
    def increment_login_attempts(self):
        self.login_attempts += 1
    
    def reset_login_attempts(self):
        self.login_attempts = 0
    
    def describe_user(self):
        print(f'First name: {self.first_name}')
        print(f'Last name: {self.last_name}')
        print(f'Gender: {self.gender}')
        print(f'Birthday: {self.birthday}')
    
    def greet_user(self):
        print(f'Hello {self.first_name.title()}. What\'s going on?')

user = User('Alice', 'Smith', 'F', '1990-01-01')
user.increment_login_attempts()
user.increment_login_attempts()
user.increment_login_attempts()
print(user.login_attempts)
user.reset_login_attempts()
print(user.login_attempts)