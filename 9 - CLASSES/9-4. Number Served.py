#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Restaurant:
    def __init__(self, restaurant_name, cuisine_type):
        self.restaurant_name = restaurant_name
        self.cuisine_type = cuisine_type
        self.number_served = 0
    
    def set_number_served(self, number_served):
        self.number_served = number_served
    
    def increment_number_served(self, number_served):
        self.number_served += number_served
    
    def describe_restaurant(self):
        print(f'Restaurant name: {self.restaurant_name}')
        print(f'Cuisine type: {self.cuisine_type}')
        print(f'Customers served: {self.number_served}')
    
    def open_restaurant(self):
        print('The resturant is open')

restaurant = Restaurant('Taco Town', 'tacos')

print(restaurant.number_served)
restaurant.number_served = 10
print(restaurant.number_served)
restaurant.set_number_served(20)
print(restaurant.number_served)
restaurant.increment_number_served(10)
print(restaurant.number_served)