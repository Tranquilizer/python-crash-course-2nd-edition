#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import choice

numbers_and_letters = (0, 1, 2, 3, 4,
                       5, 6, 7, 8, 9,
                       'a', 'b', 'c', 'd', 'e')

my_ticket = ('a', 'b', 'c', 'd')

count = 0
found = False

while not found:
    count += 1
    for i in range(4):
        if choice(numbers_and_letters) != my_ticket[i]:
            found = False
            break
        else:
            found = True

print(count)