#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from user import Privilege, Admin

privileges = Privilege(
        ["can add post", "can delete post", "can ban user"])
admin = Admin('John', 'Doe', 'M', '1980-01-01', privileges)

admin.privileges.show_privileges()