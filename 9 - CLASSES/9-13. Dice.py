#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import randint

class Die:
    def __init__(self, sides=6):
        self.sides = sides

    def roll_die(self):
        print(randint(1, self.sides))

die = Die()

print('---')
for i in range(0, 10):
    die.roll_die()

die = Die(10)

print('---')
for i in range(0, 10):
    die.roll_die()

die = Die(20)

print('---')
for i in range(0, 10):
    die.roll_die()