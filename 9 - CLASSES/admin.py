#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from user import User

class Privilege:
    def __init__(self, privileges):
        self.privileges = privileges
    
    def show_privileges(self):
        print('List of admin\'s privileges:')
        for privilege in self.privileges:
            print(f'\t- {privilege}')
            
class Admin(User):
    def __init__(self, first_name, last_name,
                 gender, birthday, privileges):
        super().__init__(first_name, last_name,gender, birthday)
        self.privileges = privileges