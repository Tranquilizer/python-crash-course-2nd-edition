#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from restaurant import Restaurant

class IceCreamStand(Restaurant):
    def __init__(self, restaurant_name, cuisine_type, flavors):
        super().__init__(restaurant_name, cuisine_type)
        self.flavors = flavors
    
    def display_flavors(self):
        print('Ice Cream Stand has these flavors:')
        for flavor in self.flavors:
            print(f'\t- {flavor}')

ice_cream_stand = IceCreamStand('Stand',
                                'ice cream',
                                ['vanilla', 'chocolate', 'strawberry'])

ice_cream_stand.describe_restaurant()
ice_cream_stand.open_restaurant()
ice_cream_stand.display_flavors()