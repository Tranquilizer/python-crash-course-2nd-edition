#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class User:
    def __init__(self, first_name, last_name, gender, birthday):
        self.first_name = first_name
        self.last_name = last_name
        self.gender = gender
        self.birthday = birthday
    
    def describe_user(self):
        print(f'First name: {self.first_name}')
        print(f'Last name: {self.last_name}')
        print(f'Gender: {self.gender}')
        print(f'Birthday: {self.birthday}')
    
    def greet_user(self):
        print(f'Hello {self.first_name.title()}. What\'s going on?')

users = [
        User('Alice', 'Smith', 'F', '1990-01-01'),
        User('Bob', 'Gordon', 'M', '1980-12-31'),
        User('Carol', 'Jenkins', 'F', '2000-06-31')
        ]

for user in users:
    user.greet_user()
    user.describe_user()