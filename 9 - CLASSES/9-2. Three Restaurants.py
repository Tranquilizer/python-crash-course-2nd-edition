#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Restaurant:
    def __init__(self, restaurant_name, cuisine_type):
        self.restaurant_name = restaurant_name
        self.cuisine_type = cuisine_type
    
    def describe_restaurant(self):
        print(f'Restaurant name: {self.restaurant_name}')
        print(f'Cuisine type: {self.cuisine_type}')
    
    def open_restaurant(self):
        print('The resturant is open')

restaurant1 = Restaurant('Taco Town', 'tacos')
restaurant2 = Restaurant('Pizza Town', 'pizzas')
restaurant3 = Restaurant('Wok Town', 'woks')

restaurant1.describe_restaurant()
restaurant2.describe_restaurant()
restaurant3.describe_restaurant()