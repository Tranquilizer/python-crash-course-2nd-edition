#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.urls import path

from . import views

app_name = 'blogs'
urlpatterns = [
    # Home page
    path('', views.index, name='index'),
    # Page for adding a post
    path('new_post/', views.new_post, name='new_post'),
    # Page for editing an entry.
    path('edit_post/<int:post_id>/', views.edit_post, name='edit_post'),
]