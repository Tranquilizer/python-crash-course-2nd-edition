#!/usr/bin/env python3
# -*- coding: utf-8 -*-

rivers = {
        'nile': 'egypt',
        'amazon': 'brazil',
        'yangtze': 'china',
        }

for key, value in rivers.items():
    print(f'The {key.title()} runs through {value.title()}.')
    
for key in rivers:
    print(f'{key.title()}')
    
for value in rivers.values():
    print(f'{value.title()}')