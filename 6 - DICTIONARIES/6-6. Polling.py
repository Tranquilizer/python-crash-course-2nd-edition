#!/usr/bin/env python3
# -*- coding: utf-8 -*-

favorite_languages = {
        'jen': 'python',
        'sarah': 'c',
        'edward': 'ruby',
        'phil': 'python',
        }
people = ['jen', 'michael', 'edward', 'paul']

for person in people:
    if person in favorite_languages:
        print('Thank you for responding.')
    else:
        print('Please, respond to the poll')