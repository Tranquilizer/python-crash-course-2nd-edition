#!/usr/bin/env python3
# -*- coding: utf-8 -*-

cities = {
        'Moscow': {
                'country': 'Russia',
                'population': 12506468,
                'fact': 'The Kremlin, as we know it,' \
                'was built by the Italians.',
                },
        'London': {
                'country': 'Great Britain',
                'population': 8908081,
                'fact': 'It was the Romans who were responsible' \
                'for the city we know today as London.',
                },
        'Paris': {
                'country': 'France',
                'population': 2206488,
                'fact': 'The name “Paris” is derived from its' \
                'early inhabitants, the Celtic Parisii tribe.',
                },
        }
        
for city, data in cities.items():
    print(city)
    for key, value in data.items():
        print(key + f': {value}')
    print()