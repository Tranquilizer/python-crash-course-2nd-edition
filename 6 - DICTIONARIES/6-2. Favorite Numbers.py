#!/usr/bin/env python3
# -*- coding: utf-8 -*-

favorite_numbers = {
        'Alice': 1,
        'Bob': 2,
        'Carol': 3,
        'Dave': 4,
        'Eve': 5,
        }

print(f"Alice - {favorite_numbers['Alice']}")
print(f"Bob - {favorite_numbers['Bob']}")
print(f"Carol - {favorite_numbers['Carol']}")
print(f"Dave - {favorite_numbers['Dave']}")
print(f"Eve - {favorite_numbers['Eve']}")