#!/usr/bin/env python3
# -*- coding: utf-8 -*-

glossary = {
        'if': 'To make a conditional statement',
        'else': 'Used in conditional statements',
        'elif': 'Used in conditional statements, same as else if',
        'in': 'To check if a value is present in a list, tuple, etc.',
        'for': 'To create a for loop',
        }

for item in glossary:
    print(f'{item}:\n\t{glossary[item]}')