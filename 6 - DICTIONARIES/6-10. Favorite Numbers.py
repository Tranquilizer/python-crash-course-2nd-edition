#!/usr/bin/env python3
# -*- coding: utf-8 -*-

favorite_numbers = {
        'Alice': [1, 6],
        'Bob': [2, 7],
        'Carol': [3],
        'Dave': [4, 8],
        'Eve': [5],
        }

for name, numbers in favorite_numbers.items():
    if len(numbers) > 1:
        print(name + '\'s favorite numbers are:')
        for number in numbers:
            print(number)
    else:
        print(name + f'\'s favorite number is {numbers[0]}')