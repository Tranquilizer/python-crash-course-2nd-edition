#!/usr/bin/env python3
# -*- coding: utf-8 -*-

first_person = {
        'first_name': 'John',
        'last_name': 'Doe',
        'age': 'n/a',
        'city': 'n/a',
        }
second_person = {
        'first_name': 'Jane',
        'last_name': 'Doe',
        'age': 'n/a',
        'city': 'n/a',
        }
third_person = {
        'first_name': 'Alice',
        'last_name': 'Monro',
        'age': '86',
        'city': 'Springfield',
        }
people = [first_person, second_person, third_person]

for person in people:
    print(person['first_name'])
    print(person['last_name'])
    print(person['age'])
    print(person['city'])