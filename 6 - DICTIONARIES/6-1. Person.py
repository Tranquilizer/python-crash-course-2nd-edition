#!/usr/bin/env python3
# -*- coding: utf-8 -*-

person = {
        'first_name': 'John',
        'last_name': 'Doe',
        'age': 'n/a',
        'city': 'n/a',
        }

print(person['first_name'])
print(person['last_name'])
print(person['age'])
print(person['city'])