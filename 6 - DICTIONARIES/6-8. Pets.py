#!/usr/bin/env python3
# -*- coding: utf-8 -*-

first_pet = {
        'kind': 'cat',
        'owner': 'Alice',
        }
second_pet = {
        'kind': 'dog',
        'owner': 'Bob',
        }
third_pet = {
        'kind': 'parrot',
        'owner': 'Carol',
        }
pets = [first_pet, second_pet, third_pet]

for pet in pets:
    print('Kind: ' + pet['kind'])
    print('Owner: ' + pet['owner'])