#!/usr/bin/env python3
# -*- coding: utf-8 -*-

favorite_places = {
        'Alice':['New York', 'Boston'],
        'Bob':['London'],
        'Carol':['Moscow', 'Saint Petersburg', 'Kazan'],
        }

for person, places in favorite_places.items():
    if len(places) > 1:
        print(f'{person}\'s favorite places are:')
        for place in places:
            print('\t' + place)
    else:
        print(f'{person}\'s favorite place is {places[0]}')